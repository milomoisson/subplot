# Big decisions made by the Subplot project

In the [2021-09-11 meeting][] we decided to start keeping a decision
log: a log of big decisions, often architectural ones. This file is
that log. Decisions should be discussed before being made so that
there is a strong consensus for them. Decisions can be changed or
overturned if they turn out to no longer be good. Overturning is a
decision.

[2021-09-11 meeting]: https://subplot.liw.fi/blog/2021/09/11/meeting/

Each decision should have its own heading. Newest decision should come
first. Updated or overturned decisions should have their section
updated to note their status, without moving them.

## Output format for `docgen`

Date: 2023-03-21

What: `subplot docgen` will only support HTML as the output format. We
no longer support PDF.

Why: We want to drop support of Pandoc for parsing markdown input
files, and this is simpler to achieve if we don't need to support PDF
output, as it relieves us from the need to produce an abstract syntax
tree in the Pandoc representation. Producing PDF without a Pandoc AST
is trickier.

Who: Daniel, Lars

## Minimum Supported Rust Version

Date: 2023-03-21

What: We decided that Subplot would support an MSRV of the version of
Rust in the Debian "testing" branch. We can bump our explicit MSRV
version when Debian gets a new version.

Who: Daniel, Lars

## Threshold for refactoring

Date: 2022-10-23

What: Instead of trying to do a whole code base tidy up, we'll
continuously do smaller refactoring changes when we see something that
needs improvement.

## Do not clear/override all environment variables

Date: 2021-10-08

What: We decided that the environment being "clean" is more the
resposibility of the caller of the test suite than of the test
suite itself. As such, we have decided to only set SHELL, HOME,
and TMPDIR. Importantly we do not override PATH etc. so that
things will work on NixOS etc.

Who: Daniel, Lars.

## Start decision log

Date: 2021-09-11

What: We decided to start keeping a decision log for big, often
architectural decisions affecting the project as a whole.

Who: Daniel, Lars.
