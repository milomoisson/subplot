## Feature request summary

(Describe the feature you would like to request)

## Example usage

Provide an example of how this feature would be used, for example if the feature
involves the markdown input, describe the markdown and how it would cause Subplot
to behave.

/label ~feature
