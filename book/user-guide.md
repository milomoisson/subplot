# Introduction

  - who is this manual for?
  - what is Subplot meant for?
  - who is Subplot meant for?
  - history of Subplot
  - public use cases of Subplot

# An overview of acceptance criteria and their verification

  - discuss acceptance criteria vs requirements; functional vs
    non-functional requirements; automated vs manual testing
  - discuss stakeholders
  - discuss different approaches for verifying that a system meets it
    criteria
  - discuss how scenarios can be used to verify acceptance criteria

# Simple example project

  - discuss how to use Subplot for some simple, but not simplistic,
    software project
  - discuss different kinds of stakeholders a project may have

# Authoring Subplot documents

  - discuss that it may be necessary to have several documents for
    different audiences, at different levels of abstraction (cf. the
    FOSDEM safety devroom talk)
  - discuss writing style to target all the different stakeholders
  - discuss mechanics and syntax
    - Markdown and supported features
    - scenarios, embedded files, examples
    - bindings
    - step implementations in various languages
    - embedded markup for diagrams
    - running docgen

# Extended example project

  - discuss how to use Subplot for a significant project, but keep it
    sufficiently high level that it doesn't get too long and tedious
    to read

# Appendix: Implementing scenario steps in Bash

  - this appendix will explain how to implement scenario steps using
    the Bash shell

# Appendix: Implementing scenario steps in Python

  - this appendix will explain how to implement scenario steps using
    the Python language

# Appendix: Implementing scenario steps in Rust

  - this appendix will explain how to implement scenario steps using
    the Rust language


# Appendix: Scenario

This is currently necessary so that codegen won't barf.

~~~scenario
when I run true
~~~
