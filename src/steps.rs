use crate::SubplotError;
use serde::{Deserialize, Serialize};
use std::fmt;

/// A scenario step.
///
/// The scenario parser creates these kinds of data structures to
/// represent the parsed scenario step. The step consists of a kind
/// (expressed as a StepKind), and the text of the step.
///
/// This is just the step as it appears in the scenario in the input
/// text. It has not been matched with a binding. See MatchedStep for
/// that.
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ScenarioStep {
    kind: StepKind,
    keyword: String,
    text: String,
}

impl ScenarioStep {
    /// Construct a new step.
    pub fn new(kind: StepKind, keyword: &str, text: &str) -> ScenarioStep {
        ScenarioStep {
            kind,
            keyword: keyword.to_owned(),
            text: text.to_owned(),
        }
    }

    /// Return the kind of a step.
    pub fn kind(&self) -> StepKind {
        self.kind
    }

    /// Return the actual textual keyword of a step.
    pub fn keyword(&self) -> &str {
        &self.keyword
    }

    /// Return the text of a step.
    pub fn text(&self) -> &str {
        &self.text
    }

    /// Construct a step from a line in a scenario.
    ///
    /// If the step uses the "and" or "but" keyword, use the default
    /// step kind instead.
    pub fn new_from_str(
        text: &str,
        default: Option<StepKind>,
    ) -> Result<ScenarioStep, SubplotError> {
        let mut words = text.split_whitespace();

        let keyword = match words.next() {
            Some(s) => s,
            _ => return Err(SubplotError::NoStepKeyword(text.to_string())),
        };

        let kind = match keyword.to_ascii_lowercase().as_str() {
            "given" => StepKind::Given,
            "when" => StepKind::When,
            "then" => StepKind::Then,
            "and" => default.ok_or(SubplotError::ContinuationTooEarly)?,
            "but" => default.ok_or(SubplotError::ContinuationTooEarly)?,
            _ => return Err(SubplotError::UnknownStepKind(keyword.to_string())),
        };

        let mut joined = String::new();
        for word in words {
            joined.push_str(word);
            joined.push(' ');
        }
        if joined.len() > 1 {
            joined.pop();
        }
        Ok(ScenarioStep::new(kind, keyword, &joined))
    }
}

impl fmt::Display for ScenarioStep {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.keyword(), self.text())
    }
}

/// The kind of scenario step we have: given, when, or then.
///
/// This needs to be extended if the Subplot language gets extended with other
/// kinds of steps. However, note that the scenario parser will hide aliases,
/// such as "and" to mean the same kind as the previous step.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum StepKind {
    /// A "given some precondition" step.
    Given,

    /// A "when something happens" step.
    When,

    /// A "then some condition" step.
    Then,
}

impl fmt::Display for StepKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            StepKind::Given => "given",
            StepKind::When => "when",
            StepKind::Then => "then",
        };
        write!(f, "{s}")
    }
}

#[cfg(test)]
mod test {
    use super::{ScenarioStep, StepKind, SubplotError};

    #[test]
    fn parses_given() {
        let step = ScenarioStep::new_from_str("GIVEN I am Tomjon", None).unwrap();
        assert_eq!(step.kind(), StepKind::Given);
        assert_eq!(step.text(), "I am Tomjon");
    }

    #[test]
    fn parses_given_with_extra_spaces() {
        let step = ScenarioStep::new_from_str("   given     I   am   Tomjon   ", None).unwrap();
        assert_eq!(step.kind(), StepKind::Given);
        assert_eq!(step.text(), "I am Tomjon");
    }

    #[test]
    fn parses_when() {
        let step = ScenarioStep::new_from_str("when I declare myself king", None).unwrap();
        assert_eq!(step.kind(), StepKind::When);
        assert_eq!(step.text(), "I declare myself king");
    }

    #[test]
    fn parses_then() {
        let step = ScenarioStep::new_from_str("thEN everyone accepts it", None).unwrap();
        assert_eq!(step.kind(), StepKind::Then);
        assert_eq!(step.text(), "everyone accepts it");
    }

    #[test]
    fn parses_and() {
        let step =
            ScenarioStep::new_from_str("and everyone accepts it", Some(StepKind::Then)).unwrap();
        assert_eq!(step.kind(), StepKind::Then);
        assert_eq!(step.text(), "everyone accepts it");
    }

    #[test]
    fn fails_to_parse_and() {
        let step = ScenarioStep::new_from_str("and everyone accepts it", None);
        assert!(step.is_err());
        match step.err() {
            None => unreachable!(),
            Some(SubplotError::ContinuationTooEarly) => (),
            Some(e) => panic!("Incorrect error: {}", e),
        }
    }
}
